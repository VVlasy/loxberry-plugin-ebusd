## [1.1.3](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/compare/v1.1.2...v1.1.3) (2022-01-09)


### Bug Fixes

* **install:** get debian codename from os-release ([7f38fde](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/7f38fde5647a32a0d83d84b72711d2c933cce928))

## [1.1.2](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/compare/v1.1.1...v1.1.2) (2022-01-09)


### Bug Fixes

* **install:** use new URL format for eBusd repository ([40c2a10](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/40c2a1071bfc7573822f38240cd0481d97ae2e04))

## [1.1.1](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/compare/v1.1.0...v1.1.1) (2022-01-09)


### Bug Fixes

* **install:** manually select arch instead of using a variable ([5a41aa0](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/5a41aa003d6ab2e9f9d16cd32a1ce86647292eea))

# [1.1.0](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/compare/v1.0.0...v1.1.0) (2020-12-28)


### Bug Fixes

* **CI:** change CI flow ([0081b5d](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/0081b5d498b38397328bbba88889e0400290f4c6))
* **CI:** fix version update script file definition ([36ee44b](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/36ee44b19b19dd408be011dd80a7286dae2b16e7))
* **install:** do not add non existent repo, just write file again ([97b3c91](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/97b3c9105d6724d85191882c4583afa9b24e6472)), closes [#6](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/issues/6)
* **install:** remove unused apt directory ([80b7773](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/80b7773b0b157abe5df03a8eb45906d43b66dbd1)), closes [#7](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/issues/7)
* **uninstall:** remove GPG key for ebusd ([155c62f](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/155c62f6e6411b04cd781457f5b6fb08d74ebee8))
* **update:** fix changed prerelease update url ([2a4485d](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/2a4485d6a7a6083a4d94c3a06a0558ac9e28f30b))


### Features

* **install:** add .list entry for buster repo ([b30e322](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/commit/b30e32272c2a33d4064b60fb645501a158c21cbf)), closes [#6](https://gitlab.com/VVlasy/loxberry-plugin-ebusd/issues/6)
