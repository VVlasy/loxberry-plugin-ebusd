<?php
require_once "loxberry_system.php";
require_once "loxberry_web.php";

// This will read your language files to the array $L
$L = LBSystem::readlanguage("language.ini");
$template_title = $L['PLUGIN.TITLE'];
$helplink = "http://www.loxwiki.eu:80/x/2wzL";
$helptemplate = "help.html";

// The Navigation Bar
$navbar[1]['Name'] = $L['NAVBAR.FIRST'];
$navbar[1]['URL'] = 'index.php';

$navbar[2]['Name'] = $L['NAVBAR.SECOND'];
$navbar[2]['URL'] = 'setmode.php';

$navbar[3]['Name'] = $L['NAVBAR.THIRD'];
$navbar[3]['URL'] = 'settings.php';
$navbar[3]['active'] = True;

LBWeb::lbheader($template_title, $helplink, $helptemplate);
?>

<style>
    .ui-flipswitch {
        background-color: grey !important;
    }

    .ui-flipswitch.ui-flipswitch-active {
        background-color: #6dac20 !important;
    }
</style>


<div>
    <table cellpadding="10" width="100%">
        <tr>
            <td><?php echo $L['SETTINGS.DAEMONONOFF'] ?></td>
            <td colspan="2">
                <fieldset>
                    <div id="status" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                         onclick="toggleDaemonState(this)">
                        <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                class="ui-flipswitch-off">Off</span>
                        <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                tabindex="-1">
                            <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                            <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                        </select></div>
                </fieldset>
            </td>
        </tr>

        <tr>
            <td><?php echo $L['SETTINGS.LAUNCHARGS'] ?></td>
            <td>
                <input id="args" value="">
            </td>
            <td style="padding: 0">
                <a id="argsButton" data-role="button"
                   href="#"
                   target="_blank" data-inline="true" data-mini="true" data-icon="action"
                   class="ui-link ui-btn ui-icon-edit ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini valueButton"
                   role="button" onclick="editArgs(this, 'args')" style="display: inline; padding-right: 0;"
                </a>
            </td>
        </tr>
    </table>
</div>

<script src="js/buttons.js"></script>
<script src="js/settings.js"></script>

<?php
// Finally print the footer
LBWeb::lbfooter();
?>
