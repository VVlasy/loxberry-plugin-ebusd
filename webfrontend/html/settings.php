<?php
require_once "loxberry_web.php";

// This will read your language files to the array $L
$L = LBSystem::readlanguage("language.ini");
$template_title = $L['PLUGIN.TITLE'];
$helplink = "http://www.loxwiki.eu:80/x/2wzL";
$helptemplate = "help.html";

LBWeb::lbheader($template_title, $helplink, $helptemplate);

// The Navigation Bar
$navbar[1]['Name'] = $L['NAVBAR.FIRST'];
$navbar[1]['URL'] = 'index.php';

$navbar[2]['Name'] = $L['NAVBAR.SECOND'];
$navbar[2]['URL'] = 'settings.php';
$navbar[2]['active'] = True;
?>




<?php
// Finally print the footer
LBWeb::lbfooter();
?>
