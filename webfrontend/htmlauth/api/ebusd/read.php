<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class EbusdRead extends ApiEndpointBase
{
    public function get(){
        $searchQuery = '';

        if(isset($_GET['search']))
            $searchQuery = ' ' . $_GET['search'];

        if(isset($_GET['update'])){
            $this->requireQueryParameters(array('circuit'));
            $updateCmds = $_GET['update'];

            if(!is_array($updateCmds))
                $this->replyError('query-param-invalid-datatype', 'Query parameter has invalid datatype', 'update has to be an array');

            $results = array();

            foreach ($updateCmds as $cmd){
                $result = '';
                $runs = 0;
                while($runs++ < 6){
                    $result = trim(shell_exec('ebusctl read -c ' . $_GET['circuit'] . ' ' . $cmd));

                    if(count(explode(";", $result)) == 2)
                        $result = explode(";", $result)[0];

                    if (strpos($result, 'ERR') === false)
                        break;
                }

                if($result == 'off' || $result == 'on')
                    $result = $result == 'on' ? 1 : 0;


                $results[$cmd] = $result;
            }


            $this->reply($results);
        }

        $findResult = '';
        $runs = 0;
        while($runs++ < 6) {
            $findResult = trim(shell_exec('ebusctl find -r' . $searchQuery));

            if (strpos($findResult, 'ERR') === false)
                break;
        }

        if(strpos($findResult, 'ERR') === true)
            $this->replyError('cmd-result-err', 'Failed to load command result', 'Please try again', null, 500);

        $data = explode("\n", $findResult);

        $circuits = array();

        foreach ($data as $line ){
            $circuit = explode(" ", $line)[0];
            $line = trim(str_replace($circuit, "", $line));

            $splitLine = explode("=", $line);
            if(count($splitLine) != 2)
                continue;

            $key = trim($splitLine[0]);
            $value = trim($splitLine[1]);

            if(!array_key_exists($circuit, $circuits))
                $circuits[$circuit] = array();

            if($value == 'off' || $value == 'on')
                $value = $value == 'on' ? 1 : 0;

            $circuits[$circuit][$key] = $value;
        }

        $this->reply($circuits);
    }
}

new EbusdRead();