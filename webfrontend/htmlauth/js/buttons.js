function enableButtonById(id) {
    enableButton(document.getElementById(id));
}

function disableButtonById(id) {
    disableButton(document.getElementById(id));
}

function enableButton(element) {
    element.classList.remove('ui-disabled');
}

function disableButton(element) {
    element.classList.add('ui-disabled');
}

function setButtonValue(element, value){
    if(value)
        element.classList.add('ui-flipswitch-active');
    else
        element.classList.remove('ui-flipswitch-active')

    let selectElement = element.children[2].children[2];
    let selectedIndex = Number(value);
    let unselectIndex = Number(!value);

    selectElement.children[unselectIndex].removeAttribute('selected');
    selectElement.children[selectedIndex].setAttribute('selected', 'selected');
}

function toggleButton(element){
    let toggled = element.classList.toggle('ui-flipswitch-active');

    setButtonValue(element, toggled);

    return toggled;
}