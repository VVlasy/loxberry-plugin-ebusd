<?php
require_once "loxberry_system.php";
require_once "loxberry_web.php";

// This will read your language files to the array $L
$L = LBSystem::readlanguage("language.ini");
$template_title = $L['PLUGIN.TITLE'];
$helplink = "http://www.loxwiki.eu:80/x/2wzL";
$helptemplate = "help.html";

// The Navigation Bar
$navbar[1]['Name'] = $L['NAVBAR.FIRST'];
$navbar[1]['URL'] = 'index.php';

$navbar[2]['Name'] = $L['NAVBAR.SECOND'];
$navbar[2]['URL'] = 'setmode.php';
$navbar[2]['active'] = True;

$navbar[3]['Name'] = $L['NAVBAR.THIRD'];
$navbar[3]['URL'] = 'settings.php';

LBWeb::lbheader($template_title, $helplink, $helptemplate);
?>

<style>
    .ui-flipswitch {
        background-color: grey !important;
    }

    .ui-flipswitch.ui-flipswitch-active {
        background-color: #6dac20 !important;
    }

    .proplist-left {
        grid-area: proplist-left;
    }

    .proplist-right {
        grid-area: proplist-right;
    }
    .valueButton{
        display: inline;
        padding-right: 0;
    }

    .wrapper {
        display: grid;
        grid-template-areas: "proplist-left" "proplist-right";
    }

    @media only screen and (min-width: 600px) {
        .wrapper {
            grid-template-columns: 50% 50%;
            grid-template-areas: "proplist-left proplist-right";
        }
    }
</style>

<div>
    <tr>
        <td><?php echo $L['SETMODE.ENABLED'] ?></td>
        <td>
            <fieldset>
                <div id="setModeOverride" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                     onclick="toggleSetmodeEnabled(this)">
                    <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                            class="ui-flipswitch-off">Off</span>
                    <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                            tabindex="-1">
                        <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                        <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                    </select></div>
            </fieldset>
        </td>
    </tr>

    <p style="color: orangered"><?php echo $L['SETMODE.OVERRIDENOTE'] ?></p>

    <br>

    <h2><?php echo $L['SETMODE.VALUES'] ?></h2>
    <div id="valuesWrapper" class="wrapper ui-disabled">
        <div class="proplist-left">
            <table cellpadding="10">
                <tr>
                    <td><?php echo $L['SETMODE.HCMODE'] ?></td>
                    <td>
                        <input id="hcmode" value="" style="display: inline">
                    </td>
                    <td style="padding: 0">
                        <a data-role="button"
                           href="#"
                           target="_blank" data-inline="true" data-mini="true" data-icon="action"
                           class="ui-link ui-btn ui-icon-edit ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini valueButton"
                           role="button" onclick="editValue(this, 'hcmode')"
                        </a>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.FLOWTEMPDESIRED'] ?></td>
                    <td>
                        <input id="flowtempdesired" value="">
                    </td>
                    <td style="padding: 0">
                        <a data-role="button"
                           href="#"
                           target="_blank" data-inline="true" data-mini="true" data-icon="action"
                           class="ui-link ui-btn ui-icon-edit ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini valueButton"
                           role="button" onclick="editValue(this, 'flowtempdesired')"
                        </a>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.DISABLEHC'] ?></td>
                    <td colspan="2">
                        <fieldset>
                            <div id="disablehc" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                                 onclick="toggleSetmodeProperty(this)">
                                <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                        class="ui-flipswitch-off">Off</span>
                                <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                        tabindex="-1">
                                    <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                                    <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                                </select></div>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.DISABLEHWCTAPPING'] ?></td>
                    <td colspan="2">
                        <fieldset>
                            <div id="disablehwctapping" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                                 onclick="toggleSetmodeProperty(this)">
                                <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                        class="ui-flipswitch-off">Off</span>
                                <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                        tabindex="-1">
                                    <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                                    <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                                </select></div>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.DISABLEHWCLOAD'] ?></td>
                    <td colspan="2">
                        <fieldset>
                            <div id="disablehwcload" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                                 onclick="toggleSetmodeProperty(this)">
                                <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                        class="ui-flipswitch-off">Off</span>
                                <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                        tabindex="-1">
                                    <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                                    <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                                </select></div>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </div>

        <div class="proplist-right">
            <table cellpadding="10">
                <tr>
                    <td><?php echo $L['SETMODE.HWCTEMPDESIRED'] ?></td>
                    <td>
                        <input id="hwctempdesired" value="">
                    </td>
                    <td style="padding: 0">
                        <a data-role="button"
                           href="#"
                           target="_blank" data-inline="true" data-mini="true" data-icon="action"
                           class="ui-link ui-btn ui-icon-edit ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini valueButton"
                           role="button" onclick="editValue(this, 'hwctempdesired')"
                        </a>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.HWCFLOWTEMPDESIRED'] ?></td>
                    <td>
                        <input id="hwcflowtempdesired" value="">
                    </td>
                    <td style="padding: 0">
                        <a data-role="button"
                           href="#"
                           target="_blank" data-inline="true" data-mini="true" data-icon="action"
                           class="ui-link ui-btn ui-icon-edit ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini valueButton"
                           role="button" onclick="editValue(this, 'hwcflowtempdesired')"
                        </a>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $L['SETMODE.REMOTECONTROLHCPUMP'] ?></td>
                    <td colspan="2">
                        <fieldset>
                            <div id="remotecontrolhcpump" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                                 onclick="toggleSetmodeProperty(this)">
                                <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                        class="ui-flipswitch-off">Off</span>
                                <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                        tabindex="-1">
                                    <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                                    <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                                </select></div>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.RELEASEBACKUP'] ?></td>
                    <td colspan="2">
                        <fieldset>
                            <div id="releasebackup" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                                 onclick="toggleSetmodeProperty(this)">
                                <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                        class="ui-flipswitch-off">Off</span>
                                <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                        tabindex="-1">
                                    <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                                    <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                                </select></div>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <td><?php echo $L['SETMODE.RELEASECOOLING'] ?></td>
                    <td colspan="2">
                        <fieldset>
                            <div id="releasecooling" class="ui-flipswitch ui-shadow-inset ui-bar-inherit ui-corner-all"
                                 onclick="toggleSetmodeProperty(this)">
                                <a href="#" class="ui-flipswitch-on ui-btn ui-shadow ui-btn-inherit">On</a><span
                                        class="ui-flipswitch-off">Off</span>
                                <select name="getdata" id="getdata" data-role="flipswitch" class="ui-flipswitch-input"
                                        tabindex="-1">
                                    <option selected="selected" value="0"><?php echo $L['SETMODE.OFF'] ?></option>
                                    <option value="1"><?php echo $L['SETMODE.ON'] ?></option>
                                </select></div>
                        </fieldset>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>

<script src="js/buttons.js"></script>
<script src="js/setmode-values.js"></script>
<script src="js/setmode.js"></script>

<?php
// Finally print the footer
LBWeb::lbfooter();
?>


