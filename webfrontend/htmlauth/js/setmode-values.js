function loadValues() {
    $.getJSON("./api/ebusd/setmode.php", function (data) {
        for (let key in data){
            let value = data[key];

            if(typeof value === 'boolean')
                setButtonValue(document.getElementById(key), value);
            else
                document.getElementById(key).setAttribute('value', value);
        }

        enableValues();
    });
}

function loadAll() {
    $.getJSON("./api/ebusd/setmodeoverride.php", function (data) {
        let button = document.getElementById('setModeOverride');
        let value = data.setModeOverride == 'active';

        setButtonValue(button, value);

        if (value)
            loadValues();
    });
}

function toggleSetmodeEnabled(element) {
    disableValues();
    disableButton(element);

    let active = toggleButton(element) ? 'active' : 'inactive';
    let data = { 'setModeOverride': active };

    $.ajax({
        type: "POST",
        url: './api/ebusd/setmodeoverride.php',
        data: JSON.stringify(data),
        success: function (response) {
            let active = response.setModeOverride == 'active';
            if (active)
                loadValues();

            setButtonValue(element, active);
        },
        complete: function() {
            enableButton(element);
        },
        dataType: "json",
        contentType : "application/json"
    });
}

function toggleSetmodeProperty(element) {
    let enabled = toggleButton(element);
    disableButton(element);

    let property = element.getAttribute('id');

    let data = {};
    data[property] = enabled;

    //console.log(JSON.stringify(data));

    $.ajax({
        element: element,
        property: property,
        type: "POST",
        url: './api/ebusd/setmode.php',
        data: JSON.stringify(data),
        success: function (response) {
            setButtonValue(element, response[property]);
        },
        complete: function(){
            enableButton(element);
        },
        dataType: "json",
        contentType : "application/json"
    });
}

function editArgs(button, property) {
    let element  = document.getElementById(property);
    disableButton(button);
    disableButton(element);

    let value = element.value;
    let data = {};

    data[property] = Number(value);
    //console.log(JSON.stringify(data));

    $.ajax({
        element: element,
        button: button,
        property: property,
        type: "POST",
        url: './api/ebusd/args.php',
        data: JSON.stringify(data),
        success: function (response) {
            element.setAttribute('value', response[property]);
        },
        complete: function() {
            enableButton(button);
            enableButton(element);
        },
        dataType: "json",
        contentType : "application/json"
    });
}

function disableValues() {
    document.getElementById('valuesWrapper').classList.add('ui-disabled');
}

function enableValues() {
    document.getElementById('valuesWrapper').classList.remove('ui-disabled');
}