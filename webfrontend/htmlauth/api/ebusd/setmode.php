<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';
require_once './SetModeStorage.php';

class EbusdSetmode extends ApiEndpointBase
{
    private $modeStorage = null;

    public function __construct()
    {
        $this->modeStorage = new SetModeStorage();

        parent::__construct();
    }

    private function checkSetModeOverrideState()
    {
        // check setmodeoverride is enabled
        $setModeOverride = trim(shell_exec('cat "$LBPCONFIG/ebusdplugin/pluginconfig.cfg" | grep -oP \'(?<=^SETMODEOVERRIDE=)(.)+\''));
        if (!$setModeOverride)
            $this->replyError('setmodeoverride-disabled', 'SetModeOverride is not enabled');
    }

    public function get()
    {
        $this->checkSetModeOverrideState();
        $this->reply($this->modeStorage);
    }

    public function post()
    {
        $this->checkSetModeOverrideState();

        // below parse response
        if (isset($this->request->hcmode))
            $this->modeStorage->hcmode = (int)$this->request->hcmode;

        if (isset($this->request->flowtempdesired))
            $this->modeStorage->flowtempdesired = (float)$this->request->flowtempdesired;

        if (isset($this->request->hwctempdesired))
            $this->modeStorage->hwctempdesired = (float)$this->request->hwctempdesired;

        if (isset($this->request->hwcflowtempdesired))
            $this->modeStorage->hwcflowtempdesired = (float)$this->request->hwcflowtempdesired;

        if (isset($this->request->disablehc))
            $this->modeStorage->disablehc = (bool)$this->request->disablehc;

        if (isset($this->request->disablehwctapping))
            $this->modeStorage->disablehwctapping = (bool)$this->request->disablehwctapping;

        if (isset($this->request->disablehwcload))
            $this->modeStorage->disablehwcload = (bool)$this->request->disablehwcload;

        if (isset($this->request->remotecontrolhcpump))
            $this->modeStorage->remotecontrolhcpump = (bool)$this->request->remotecontrolhcpump;

        if (isset($this->request->releasebackup))
            $this->modeStorage->releasebackup = (bool)$this->request->releasebackup;

        if (isset($this->request->releasecooling))
            $this->modeStorage->releasecooling = (bool)$this->request->releasecooling;

        $this->modeStorage->save();

        $cmd = 'ebusctl write -c bai SetModeOverride "' . $this->modeStorage->serialize() . '"';

        shell_exec($cmd);

        $this->get();
    }
}

new EbusdSetmode();