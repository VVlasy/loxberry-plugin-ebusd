#!/bin/bash
sudo systemctl start ebusd
sleep 1

#if setmodeoverride enabled, send define command
SETMODE=$(cat "$LBPCONFIG/ebusdplugin/pluginconfig.cfg" | grep -oP '(?<=^SETMODEOVERRIDE=)(.)+')

if [[ $SETMODE == 1 ]] ; then
    ebusctl define wi,bai,SetModeOverride,Betriebsart,,08,B510,00,hcmode,,UCH,,,,flowtempdesired,,D1C,,,,hwctempdesired,,D1C,,,,hwcflowtempdesired,,UCH,,,,,,IGN:1,,,,disablehc,,BI0,,,,disablehwctapping,,BI1,,,,disablehwcload,,BI2,,,,,,IGN:1,,,,remoteControlHcPump,,BI0,,,,releaseBackup,,BI1,,,,releaseCooling,,BI2
fi

#TODO: Remove
#start php server and save pid to pluginconfig.cfg
#nohup php "$LBPBIN/ebusdplugin/socket_server.php" </dev/null 2>&1 &
#PHPPID=$!

#sed -i -e "s/\(^PID=\).\+/\1$PHPPID/" "$LBPCONFIG/ebusdplugin/serverconfig.cfg"
