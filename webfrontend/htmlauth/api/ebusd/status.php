<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class EbusdStatus extends ApiEndpointBase
{
    public function get(){
        $data = $this->getStatus();

        $this->reply($data);
    }

    public function post(){
        $this->requireRequestParameters(array('status'));

        if ($this->request->status != 'active' && $this->request->status != 'inactive')
            $this->replyError('invalid-value-error', 'Received invalid value',
                'Accepted status values are \'active\' or \'inactive\'');

        if($this->request->status == 'active')
            shell_exec('sudo $LBPBIN/ebusdplugin/start.sh');
        else
            shell_exec('sudo $LBPBIN/ebusdplugin/stop.sh');

        $this->get();
    }

    private function getStatus(){
        $daemonStatus=trim(shell_exec('systemctl is-active ebusd'));

        return array('status' => $daemonStatus);
    }
}

new EbusdStatus();