<?php

namespace ebusd\Api;

abstract class ApiEndpointBase
{
    /**
     * Object containing all incoming request params
     * @var object
     */
    protected $method;
    protected $args;
    protected $request;

    public function __construct()
    {
        $this->method = $_SERVER['REQUEST_METHOD'];

        // get the request body
        if (!empty($_REQUEST)) {
            // convert to object for consistency
            $this->request = json_decode(json_encode($_REQUEST));
        } else {
            // already object
            $this->request = json_decode(file_get_contents('php://input'));
        }

        if (method_exists($this, $this->method)) {
            $function = $this->method;
            $this->$function();
        } else {
            $restMethods = array("GET", "POST", "PUT", "PATCH", "DELETE", "OPTIONS");
            $supportedRestMethods = array();

            foreach (get_class_methods($this) as $key => $value) {
                if (in_array(strtoupper($value), $restMethods)) {
                    array_push($supportedRestMethods, strtoupper($value));
                }
            }

            $message = array("supported_methods" => $supportedRestMethods);
            $this->replyError('unsupported-method', 'Unsupported method', $this->method . ' is not supported by this endpoint',
                null, 400, $message);
        }
    }

    /**
     * Routes incoming requests to the corresponding method
     *
     * Converts $_REQUEST to an object, then checks for the given action and
     * calls that method. All the request parameters are stored under
     * $this->request.
     */

    /**
     * Returns JSON data with HTTP status code
     *
     * @param  array $data - data to return
     * @param  int $status - HTTP status code
     * @return JSON
     */
    protected function reply($data, $status = 200)
    {
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');
        if ($status >= 400)
            $this->replyError('invalid-method-error', 'Incorrect method used', 'Used reply() instead of replyError()', null, 500);

        header($protocol . ' ' . $status);
        header('Content-Type: application/json');
        echo json_encode($data);
        exit;
    }

    protected function replyError($type, $title, $detail, $instance=null, $status = 400, $otherData=null)
    {
        $protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.1');
        if ($status < 400)
            $this->replyError('invalid-method-error', 'Incorrect method used', 'Used replyError() instead of reply()', null, 500);

        header($protocol . ' ' . $status);
        header('Content-Type: application/problem+json');

        $method = 'http://';
        if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
            $method = 'https://';
        }

        $data = array();
        $data['type'] = $method . $_SERVER['HTTP_HOST'] . '/api/docs/error/' . $type;

        if(isset($title) && trim($title) !== '')
            $data['title'] = $title;

        if(isset($detail) && trim($detail) !== '')
            $data['detail'] = $detail;

        if(isset($instance) && trim($instance) !== '')
            $data['instance'] = $title;

        if(isset($otherData))
            $data = array_merge($data, $otherData);

        echo json_encode($data);
        exit;
    }

    protected function requireRequestParameters($paramsArray)
    {
        $missingParams = array();

        foreach ($paramsArray as $key => $value) {
            if (!isset($this->request->$value)) {
                array_push($missingParams, $value);
            }
        }

        if (count($missingParams) > 0) {
            $this->replyError('missing-parameters', 'Missing parameters required by endpoint', null,
                null, 400, array(
                    "missing_parameters" => $missingParams,
                ));
        }
    }

    protected function requireQueryParameters($paramsArray)
    {
        $missingParams = array();

        foreach ($paramsArray as $key => $value) {
            if (!isset($_GET[$value])) {
                array_push($missingParams, $value);
            }
        }

        if (count($missingParams) > 0) {
            $this->replyError('missing-query-parameters', 'Missing query parameters required by endpoint', null,
                null, 400, array(
                    "missing_query_parameters" => $missingParams,
                ));
        }
    }

    protected function requirePostBodyParameters($paramsArray){
        $missingParams = array();

        // Post body only works when request is POST, seems obvious, someone might forget
        if ($this->method != "POST") {
            $this->replyError('invalid-method-error', 'Incorrect method used', 'requirePostBodyParams() called for non POST request',
                null, 500);
        }

        foreach ($paramsArray as $key => $value) {
            if (!isset($_POST[$value])) {
                array_push($missingParams, $value);
            }
        }

        if (count($missingParams) > 0) {
            $this->replyError('missing-post-parameters', 'Missing POST parameters required by endpoint', null,
                null, 400, array(
                    "missing_post_parameters" => $missingParams,
                ));
        }
    }
}
?>