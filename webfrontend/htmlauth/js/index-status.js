function loadStatus() {
    $.getJSON("./api/ebusd/info.php", function (data) {
        if(Object.keys(data).indexOf('ERR') != -1 || Object.keys(data).indexOf('ERR: command not found') != -1 || Object.values(data).indexOf('ERR: command not found') != -1){
            loadStatus();
            console.warn('Failed to load info, ebusd is overloaded. Retrying...');
            return;
        }

        $('#statusloading').remove();

        let stateableInfos = ['status', 'signal']
        let skippedInfos = ['setModeOverride'];

        let i = 0;
        for (let key in data) {
            if (skippedInfos.indexOf(key) != -1)
                continue;

            let row = $('#statusItem').clone();
            let rowContent = $(row.prop('content'));

            rowContent.find('.info-name').html(key);

            rowContent.find('.info-value').html(data[key]);

            if(stateableInfos.indexOf(key) == -1)
                rowContent.find('.info-state').remove();
            else {
                rowContent.find('.info-value').remove();

                rowContent.find('.info-state').html(data[key]);

                if((key == 'status' && data[key] != 'active') || (key == 'signal' && data[key] != 'acquired'))
                    rowContent.find('.info-state').css("background-color", "red");
            }

            if(i % 2 == 0)
                $('#statusTableLeft').append(row.html());
            else
                $('#statusTableRight').append(row.html());
            i++;
        }
    });
}