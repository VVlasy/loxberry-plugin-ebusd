<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class EbusdArgs extends ApiEndpointBase
{
    public function get(){
        $data = $this->getArgs();

        $this->reply($data);
    }

    public function post(){
        $this->requireRequestParameters(array('args'));

        shell_exec('sudo $LBPBIN/ebusdplugin/changeargs.sh "' . $this->request->args . '"');

        $data = $this->getArgs();

        $this->reply($data);
    }

    private function getArgs(){
        $startupArgs=trim(shell_exec('cat "/etc/default/ebusd" | grep -oP \'(?<=^EBUSD_OPTS=")([^"])+\' '));
        return array('args' => $startupArgs);
    }
}

new EbusdArgs();