<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class EbusdInfo extends ApiEndpointBase
{
    public function get(){
        $startupArgs=trim(shell_exec('cat "/etc/default/ebusd" | grep -oP \'(?<=^EBUSD_OPTS=")([^"])+\' '));
        $setModeOverride=trim(shell_exec('cat "$LBPCONFIG/ebusdplugin/pluginconfig.cfg" | grep -oP \'(?<=^SETMODEOVERRIDE=)(.)+\''));
        $daemonStatus=trim(shell_exec('systemctl is-active ebusd'));

        $data = array('status' => $daemonStatus, 'args' => $startupArgs,
            'setModeOverride' => $setModeOverride == '1' ? 'active' : 'inactive');

        if ($daemonStatus == 'inactive'){
            $this->reply($data);
            return;
        }

        $infoResult=trim(shell_exec('ebusctl info'));
        $infoData = explode("\n", $infoResult);

        $info = array();

        foreach ($infoData as $line ){
            $splitLine = explode(":", $line);
            if(count($splitLine) != 2)
                continue;

            $key = trim($splitLine[0]);
            $value = trim($splitLine[1]);

            $info[$key] = $value;
        }

        $this->reply(array_merge($data, $info));
    }
}

new EbusdInfo();