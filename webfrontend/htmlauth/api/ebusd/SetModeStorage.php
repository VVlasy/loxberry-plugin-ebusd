<?php

namespace ebusd\Api;

require_once "loxberry_system.php";

class SetModeStorage
{
    public $hcmode;
    public $flowtempdesired;
    public $hwctempdesired;
    public $hwcflowtempdesired;
    public $disablehc;
    public $disablehwctapping;
    public $disablehwcload;
    public $remotecontrolhcpump;
    public $releasebackup;
    public $releasecooling;

    public function __construct()
    {
        $this->load();
    }

    public function load(){
        $data = json_decode(file_get_contents(LBPDATADIR . '/setModeOverride.dat'), true);

        $this->hcmode = $data['hcmode'];
        $this->flowtempdesired = $data['flowtempdesired'];
        $this->hwctempdesired = $data['hwctempdesired'];
        $this->hwcflowtempdesired = $data['hwcflowtempdesired'];
        $this->disablehc = $data['disablehc'];
        $this->disablehwctapping = $data['disablehwctapping'];
        $this->disablehwcload = $data['disablehwcload'];
        $this->remotecontrolhcpump = $data['remotecontrolhcpump'];
        $this->releasebackup = $data['releasebackup'];
        $this->releasecooling = $data['releasecooling'];
    }

    public function save(){
        file_put_contents(LBPDATADIR . '/setModeOverride.dat', json_encode($this));
    }

    public function serialize(){
        return (int)$this->hcmode . ';' . (int)$this->flowtempdesired . ';' . (int)$this->hwctempdesired . ';' .
            (int)$this->hwcflowtempdesired . ';' . (int)$this->disablehc . ';' . (int)$this->disablehwctapping . ';' .
            (int)$this->disablehwcload . ';' . (int)$this->remotecontrolhcpump . ';' . (int)$this->releasebackup . ';' .
            (int)$this->releasecooling . ';';
    }
}