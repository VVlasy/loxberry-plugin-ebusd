function loadValues() {
    $.getJSON("./api/ebusd/status.php", function (data) {
        setButtonValue(document.getElementById('status'), data.status == 'active');
    });

    $.getJSON("./api/ebusd/args.php", function (data) {
        document.getElementById('args').setAttribute('value', data.args);
    });
}

function toggleDaemonState(element) {
    let enabled = toggleButton(element) ? 'active' : 'inactive';
    disableButton(element);
    disableButton(document.getElementById('args'));
    disableButton(document.getElementById('argsButton'));

    let property = element.getAttribute('id');

    let data = {};
    data[property] = enabled;

    //console.log(JSON.stringify(data));

    $.ajax({
        element: element,
        property: property,
        type: "POST",
        url: './api/ebusd/status.php',
        data: JSON.stringify(data),
        success: function (response) {
            setButtonValue(element, response[property] == 'active');
        },
        complete: function(){
            enableButton(element);
            enableButton(document.getElementById('args'));
            enableButton(document.getElementById('argsButton'));
        },
        dataType: "json",
        contentType : "application/json"
    });
}

function editArgs(button, property) {
        let element  = document.getElementById(property);
        disableButton(button);
        disableButton(element);
        disableButton(document.getElementById('status'));

        let value = element.value;
        let data = {};

        data[property] = value;
        //console.log(JSON.stringify(data));

        $.ajax({
            element: element,
            button: button,
            property: property,
            type: "POST",
            url: './api/ebusd/args.php',
            data: JSON.stringify(data),
            success: function (response) {
                element.setAttribute('value', response[property]);
            },
            complete: function() {
                enableButton(button);
                enableButton(element);
                enableButton(document.getElementById('status'));
            },
            dataType: "json",
            contentType : "application/json"
        });
}

$.ajaxSetup({ cache: false });
loadValues();