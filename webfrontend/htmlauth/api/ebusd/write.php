<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class EbusdWrite extends ApiEndpointBase
{
    public function get(){
        $searchQuery = '';

        if(isset($_GET['search']))
            $searchQuery = ' ' . $_GET['search'];

        $findResult = '';
        $runs = 0;
        while($runs++ < 6) {
            $findResult = trim(shell_exec('ebusctl find -w' . $searchQuery));

            if (strpos($findResult, 'ERR') === false)
                break;
        }

        if(strpos($findResult, 'ERR') !== false)
            return $this->replyError('cmd-result-err', 'Failed to load command result', 'Please try again', null, 500);

        $data = explode("\n", $findResult);

        $circuits = array();

        foreach ($data as $line ){
            $circuit = explode(" ", $line)[0];
            $line = trim(str_replace($circuit, "", $line));

            $splitLine = explode("=", $line);
            if(count($splitLine) != 2)
                continue;

            $key = trim($splitLine[0]);
            $value = trim($splitLine[1]);

            if(!array_key_exists($circuit, $circuits))
                $circuits[$circuit] = array();

            $circuits[$circuit][$key] = $value;
        }

        $this->reply($circuits);
    }

    public function post(){
        foreach ($this->request as $circuit => $params) {
            foreach ($params as $param => $value){
                //echo "Circuit: " . $circuit . " | Param: " . $param . " | Value: " . $value .  "\r\n";
                $writeResult = '';
                $runs = 0;
                while($runs++ < 6) {
                    $writeResult = trim(shell_exec('ebusctl write -c ' . $circuit . ' ' . $param . ' ' . $value));

                    if (strpos($writeResult, 'ERR') === false)
                        break;
                }

                // Overwrite original value with result
                $this->request->$circuit->$param = strtok($writeResult, "\n");
            }
        }

        $this->reply($this->request);
    }
}

new EbusdWrite();