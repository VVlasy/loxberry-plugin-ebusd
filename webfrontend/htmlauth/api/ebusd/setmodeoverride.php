<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';
require_once './SetModeStorage.php';

class EbusdSetmodeoverride extends ApiEndpointBase
{

    public function get()
    {
        $this->reply(array('setModeOverride' => $this->loadSetmodeoverrideValue() == '1' ? 'active' : 'inactive'));
    }

    public function post(){
        $this->requireRequestParameters(array('setModeOverride'));

        // Do not restart and do all the things unless value required and value set differ
        $override = $this->loadSetmodeoverrideValue();
        if(($override == '1' && $this->request->setModeOverride == 'active') || ($override == '0' && $this->request->setModeOverride == 'inactive'))
            $this->get();

        $wasRunning = trim(shell_exec('systemctl is-active ebusd'));

        if($wasRunning == 'active')
            shell_exec('sudo $LBPBIN/ebusdplugin/stop.sh');

        if($this->request->setModeOverride == 'active')
            shell_exec('sed -i -e "s/\(^SETMODEOVERRIDE=\).\+/\11/" "$LBPCONFIG/ebusdplugin/pluginconfig.cfg"');
        else
            shell_exec('sed -i -e "s/\(^SETMODEOVERRIDE=\).\+/\10/" "$LBPCONFIG/ebusdplugin/pluginconfig.cfg"');

        if($wasRunning == 'active')
            shell_exec('sudo $LBPBIN/ebusdplugin/start.sh');

        $this->get();
    }

    public function loadSetmodeoverrideValue(){
        $setModeOverride=trim(shell_exec('cat "$LBPCONFIG/ebusdplugin/pluginconfig.cfg" | grep -oP \'(?<=^SETMODEOVERRIDE=)(.)+\''));
        return $setModeOverride;
    }
}

new EbusdSetmodeoverride();