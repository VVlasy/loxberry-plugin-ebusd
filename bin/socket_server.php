<?php
set_time_limit(0); // disable timeout
ob_implicit_flush(); // disable output caching

// Settings
$address = '0';
$port = shell_exec('cat "$LBPCONFIG/ebusdplugin/serverconfig.cfg" | grep -oP \'(?<=^PORT=)(.)+\'');


/*
    function socket_create ( int $domain , int $type , int $protocol )
    $domain can be AF_INET, AF_INET6 for IPV6 , AF_UNIX for Local communication protocol
    $protocol can be SOL_TCP, SOL_UDP  (TCP/UDP)
    @returns true on success
*/

if (($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
    echo "Couldn't create socket".socket_strerror(socket_last_error())."\n";
}


if (socket_bind($socket, $address, $port) === false) {
    #echo "Bind Error ".socket_strerror(socket_last_error($socket)) ."\n";
}

if (socket_listen($socket, 5) === false) {
    #echo "Listen Failed ".socket_strerror(socket_last_error($socket)) . "\n";
}

do {
    if (($msgsock = socket_accept($socket)) === false) {
        #echo "Error: socket_accept: " . socket_strerror(socket_last_error($socket)) . "\n";
        continue;
    }

    // Listen to user input
    do {
        if (false === ($buf = socket_read($msgsock, 2048, PHP_NORMAL_READ))) {
            #echo "socket read error: ".socket_strerror(socket_last_error($msgsock)) . "\n";
            break;
        }
        if (!$buf = trim($buf)) {
            continue;
        }

        // handle user data

        // Reply to user with their message
        $response = "Response: '$buf'.";
        socket_write($msgsock, $response, strlen($response));
        break;
    } while (true);
    socket_shutdown($msgsock, 2);
    socket_close($msgsock);
} while (true);

socket_close($socket);