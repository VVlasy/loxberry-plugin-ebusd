#!/bin/bash
STARTFILEPATH="$LBPBIN/ebusdplugin/start.sh"
STOPFILEPATH="$LBPBIN/ebusdplugin/stop.sh"

DAEMONSTATUS="$(sudo systemctl is-active ebusd)"

if [[ $# -ne 1 ]] ; then
    exit 1
fi

if [[ $DAEMONSTATUS = "active" ]] ; then
    ${STOPFILEPATH}
fi

sed -i -e "s|\(^EBUSD_OPTS=\"\)[^\"]*|\1$1|" "/etc/default/ebusd"

if [[ $DAEMONSTATUS = "active" ]] ; then
    ${STARTFILEPATH}
fi