function createWriteParamBody(nname, nvalue, ncircuit) {
    template = $('#writeParamTemplate').clone();
    var node = template.prop('content');

    var name = $(node).find('.write-p-name');
    name.text(nname);

    var value = $(node).find('.write-p-value-input');
    value.attr('id', 'write-p-value-input');

    var state = $(node).find('.write-p-state');

    if (nvalue == 'no data stored') {
        state.css("background-color", "orange");

        $(node).find('.write-p-state-ok').remove();
        $(node).find('.write-p-state-error').remove();
    } else if (nvalue.toString().includes('ERR')) {
        state.css("background-color", "red");

        $(node).find('.write-p-state-ok').remove();
        $(node).find('.write-p-state-unknown').remove();
    } else {
        $(node).find('.write-p-state-unknown').remove();
        $(node).find('.write-p-state-error').remove();

        value.attr('value', nvalue);
    }

    var btn = $(node).find('.ui-btn');
    btn.attr("onclick", "writeParam('" + ncircuit + "', '" + nname + "')");

    return template;
}

function combineCircuits(target, source) {
    target = JSON.parse(JSON.stringify(target));
    source = JSON.parse(JSON.stringify(source));

    let combined = Object.assign({}, source, target);
    let circuits = Object.keys(source);

    for (let i = 0; i < circuits.length; ++i) {
        let circuit = source[circuits[i]];
        for (let key in circuit) {
            if (Object.keys(combined[circuits[i]]).indexOf(key) == -1) {
                //console.log(key);
                target[circuits[i]][key] = source[circuits[i]][key];
            }
        }
    }

    return combined;
}

function loadCombineWriteParams() {
    // load available write vars, match them against existing read vars
    $.getJSON("./api/ebusd/write.php", function (data) {
        let circuits = Object.keys(data);

        let combinedCircuits = combineCircuits(readCircuits, data);

        for (let i = 0; i < circuits.length; ++i) {
            let circuitName = circuits[i];

            // make sure write circuit has a category for read circuit already defined
            if (readCircuits[circuitName] != undefined) {
                //console.log(data[circuitName]);
                for (let key in data[circuitName]) {
                    // only if read var exists for write var
                    if (readCircuits[circuitName][key] != undefined) {
                        //console.log(key + ': ' + readCircuits[circuitName][key]);
                        let buttonTemplate = $('#writeButton').clone();
                        let btn = $(buttonTemplate.prop('content')).find('.ui-btn');
                        btn.attr("onclick", "writeParam('" + circuitName + "', '" + key + "')");

                        let row = $('#read-' + circuitName + '-' + key);
                        row.find('.read-p-buttons').append(buttonTemplate.html());

                        let value = row.find('.read-p-value').html();

                        if (value == 'no data stored')
                            value = '';

                        row.find('.read-p-value').html('<input id="read-p-value-input" value="' + value + '">');
                    } else {
                        // if key not exists add it the correct place with just write
                        let keys = Object.keys(combinedCircuits[circuitName]).sort(function (a, b) {
                            return a.toLowerCase().localeCompare(b.toLowerCase());
                        });

                        let index = keys.indexOf(key);
                        let paramBefore = Object.keys(combinedCircuits[circuitName])[index - 1];

                        let row = createWriteParamBody(key, combinedCircuits[circuitName][key], circuitName);

                        $('#read-' + circuitName + '-' + paramBefore).after(row.html());
                    }
                }
            } else {
                // if circuit not exists add it append it and add its params
                fillReadContainerTemplate(circuitName);

                var circuit = data[circuitName];

                //
                var propertis = Object.keys(circuit);

                var cindex;
                for (cindex = 0; cindex < propertis.length; ++cindex) {
                    var name = propertis[cindex];
                    var value = circuit[name];

                    if (typeof value.split !== "undefined" && value.split(';').length == 2)
                        value = value.split(';')[0];

                    let row = createWriteParamBody(name, value, circuitName);

                    var contain = $("<tr>", {id: "write-" + circuitName + "-" + name});
                    contain.append(row.html());
                    $('#readTable-' + circuitName).append(contain.html());
                }
            }
        }

    });
}

function writeParam(circuit, param) {
    // first try to get value for read/write rows
    let rowPrefix = 'read';

    let row = $('#read-' + circuit + '-' + param);
    let rowInput = row.find('#read-p-value-input');

    if (!rowInput.length) {
        row = $('#write-' + circuit + '-' + param);
        rowInput = row.find('#write-p-value-input');
        rowPrefix = 'write';
    }

    let params = {};
    params[param] = rowInput.val();

    let data = {}
    data[circuit] = params;

    //console.log(JSON.stringify(data));

    $.ajax({
        row: row,
        rowPrefix: rowPrefix,
        circuit: circuit,
        param: param,
        type: "POST",
        url: './api/ebusd/write.php',
        data: JSON.stringify(data),
        success: function (response) {
            let value = response[this.circuit][this.param];
            if (typeof (value) == "undefined")
                value = "no data stored";

            let stateTemplate = $('#paramState').clone();
            let stateColor = '#6DAC20';

            if (value == 'no data stored') {
                stateColor = "orange";

                $(stateTemplate.prop('content')).find('.p-state-ok').remove();
                $(stateTemplate.prop('content')).find('.p-state-error').remove();
            } else if (value.toString().includes('ERR')) {
                stateColor = "red";

                $(stateTemplate.prop('content')).find('.p-state-ok').remove();
                $(stateTemplate.prop('content')).find('.p-state-unknown').remove();

                alert(value); // TODO: Show nicer alert
            } else {
                $(stateTemplate.prop('content')).find('.p-state-unknown').remove();
                $(stateTemplate.prop('content')).find('.p-state-error').remove();
            }

            this.row.find('.' + this.rowPrefix + '-p-state').html(stateTemplate.html());
            this.row.find('.' + this.rowPrefix + '-p-state').css('background-color', stateColor);
        },
        dataType: "json",
        contentType: "application/json"
    });
}