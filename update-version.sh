#!/bin/bash
echo "Branch is $CI_COMMIT_BRANCH"

version=$1
if [ "$CI_COMMIT_BRANCH" = "development" ] ; then
    version=$(echo "$version" | grep -o -P "[0-9]+.[0-9]+.[0-9]+")
fi

echo "Version is $version"

if [ "$CI_COMMIT_BRANCH" = "master" ] || [ "$CI_COMMIT_BRANCH" = "development" ] ; then
   sed -i -e "s/\(^VERSION=\)[0-9]\+.[0-9]\+.[0-9]\+/\1$version/" plugin.cfg

#if prerelease
    if [ "$CI_COMMIT_BRANCH" = "development" ] ; then
        sed -i -e "s/\(^VERSION=\)[0-9]\+.[0-9]\+.[0-9]\+/\1$version/" prerelease.cfg
    fi

#if release
    if [ "$CI_COMMIT_BRANCH" = "master" ] ; then
        sed -i -e "s/\(^VERSION=\)[0-9]\+.[0-9]\+.[0-9]\+/\1$version/" release.cfg
    fi
fi