<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class ServerInfo extends ApiEndpointBase
{
    public function get()
    {
        $serverPid=trim(shell_exec('cat "$LBPCONFIG/ebusdplugin/serverconfig.cfg" | grep -oP \'(?<=^PID=)(.)+\''));

        $statCmd='test -e /proc/' . $serverPid . ' && echo active || echo inactive';
        $serverStatus=trim(shell_exec($statCmd));

        $data = array('status' => $serverStatus);

        $this->reply($data);
    }
}

new ServerInfo();
