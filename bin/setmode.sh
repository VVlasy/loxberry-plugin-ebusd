#!/bin/bash
command="ebusctl write -c bai SetModeOverride"
args=""

# Do like this
readarray -d ',' lines < "$LBPDATA/ebusdplugin/setModeOverride.dat"

for i in {0..9}
  do
     line=${lines[$i]}
     line=${line//,/} #remove colon
     line=${line// /} #remove space
     line=${line#*:} #extract value

     line=${line//false/0} #bool to int - false
     line=${line//true/1} #bool to int - true
     line=${line//$'\n'/} #remove newlines
     line=${line//$'}'/} #remove end of line

     args="$args$line;"
 done

command="$command \"$args\""

eval $command