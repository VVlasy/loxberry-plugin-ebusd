function fillReadContainerTemplate(nname) {
    template = $('#readContainerTemplate').clone();
    var node = template.prop('content');

    var name = $(node).find('.read-c-name');
    name.text(nname);

    var table = $(node).find('.read-c-table');
    table.attr('id', 'readTable-' + nname);

    $('#readBlockBody').append(template.html());
}

function createReadParamBody(parent, nname, nvalue, ncircuit) {
    template = $('#readParamTemplate').clone();
    var node = template.prop('content');

    var name = $(node).find('.read-p-name');
    name.text(nname);

    var value = $(node).find('.read-p-value');
    value.text(nvalue);

    var state = $(node).find('.read-p-state');

    if (nvalue == 'no data stored') {
        state.css("background-color", "orange");

        $(node).find('.read-p-state-ok').remove();
        $(node).find('.read-p-state-error').remove();
    } else if (nvalue.toString().includes('ERR')) {
        state.css("background-color", "red");

        $(node).find('.read-p-state-ok').remove();
        $(node).find('.read-p-state-unknown').remove();
    } else {
        $(node).find('.read-p-state-unknown').remove();
        $(node).find('.read-p-state-error').remove();
    }

    var btn = $(node).find('.ui-btn');
    btn.attr("onclick", "updateReadParam('" + ncircuit + "', '" + nname + "')");

    parent.append(template.html());
}

function fillReadParamTemplate(nname, nvalue, ncircuit) {
    var contain = $("<tr>", {id: "read-" + ncircuit + "-" + nname});

    createReadParamBody(contain, nname, nvalue, ncircuit)

    $('#readTable-' + ncircuit).append(contain);
}

function loadReadParams() {
    $('#readBlockBody').html('<h4>' + loadingText + '</h4>');

    $.getJSON("./api/ebusd/status.php", function (status) {
        if(status.status == 'inactive')
        {
            $('#coll_ebusd_read').remove();
            return;
        }

        $.getJSON("./api/ebusd/read.php", function (data) {
            $('#readBlockBody').empty();

            if (data.length == 0) {
                console.warn("Received no values back, ebus is overloaded. Retrying...");
                loadReadParams();
                return;
            }

            var circuits = Object.keys(data);
            readCircuits = data;

            var dindex;
            for (dindex = 0; dindex < circuits.length; ++dindex) {
                var circuitName = circuits[dindex];
                fillReadContainerTemplate(circuitName);
            }

            for (dindex = 0; dindex < circuits.length; ++dindex) {
                var circuitName = circuits[dindex];
                var circuit = data[circuitName];

                //
                var propertis = Object.keys(circuit);

                var cindex;
                for (cindex = 0; cindex < propertis.length; ++cindex) {
                    var name = propertis[cindex];
                    var value = circuit[name];

                    if (typeof value.split !== "undefined" && value.split(';').length == 2)
                        value = value.split(';')[0];

                    fillReadParamTemplate(name, value, circuitName);
                }
            }

            loadCombineWriteParams();
        });
    });
}

function updateReadParam(circuit, param) {
    $.getJSON("./api/ebusd/read.php?circuit=" + circuit + "&update[]=" + param, function (data) {
        let value = data[param];
        let stateTemplate = $('#paramState').clone();
        let stateColor = '#6DAC20';

        if (value == 'no data stored') {
            stateColor = "orange";

            $(stateTemplate.prop('content')).find('.p-state-ok').remove();
            $(stateTemplate.prop('content')).find('.p-state-error').remove();
        } else if (value.toString().includes('ERR')) {
            stateColor = "red";

            $(stateTemplate.prop('content')).find('.p-state-ok').remove();
            $(stateTemplate.prop('content')).find('.p-state-unknown').remove();
        } else {
            $(stateTemplate.prop('content')).find('.p-state-unknown').remove();
            $(stateTemplate.prop('content')).find('.p-state-error').remove();
        }

        var row = $("#read-" + circuit + "-" + param);

        let rowState = row.find('.read-p-state');
        rowState.html(stateTemplate.html());
        rowState.css('background-color', stateColor);

        let rowValue = row.find('.read-p-value');
        let rowValueInput = row.find('#read-p-value-input');
        if (rowValueInput.length)
            rowValueInput.val(value);
        else
            rowValue.html(value);
    });
}

function readAllParams() {
    let circuits = Object.keys(readCircuits);

    for (let i = 0; i < circuits.length; ++i) {
        let circuit = readCircuits[circuits[i]];
        for (let key in circuit) {
            updateReadParam(circuits[i], key);
        }
    }
}