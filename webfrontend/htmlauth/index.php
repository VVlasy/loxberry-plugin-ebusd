<?php
require_once "loxberry_system.php";
require_once "loxberry_web.php";

// This will read your language files to the array $L
$L = LBSystem::readlanguage("language.ini");
$template_title = $L['PLUGIN.TITLE'];
$helplink = "http://www.loxwiki.eu:80/x/2wzL";
$helptemplate = "help.html";

// The Navigation Bar
$navbar[1]['Name'] = $L['NAVBAR.FIRST'];
$navbar[1]['URL'] = 'index.php';
$navbar[1]['active'] = True;

$navbar[2]['Name'] = $L['NAVBAR.SECOND'];
$navbar[2]['URL'] = 'setmode.php';

$navbar[3]['Name'] = $L['NAVBAR.THIRD'];
$navbar[3]['URL'] = 'settings.php';

LBWeb::lbheader($template_title, $helplink, $helptemplate);
?>
<style>
    .statuslist-left {
        grid-area: statuslist-left;
    }

    .statuslist-right {
        grid-area: statuslist-right;
    }

    .wrapper {
        display: grid;
        grid-template-areas: "statuslist-left" "statuslist-right";
        overflow: auto;
    }

    #writeBlockBody, #readBlockBody {
        overflow: auto
    }

    @media only screen and (min-width: 800px) {
        .wrapper {
            grid-template-columns: 50% 50%;
            grid-template-areas: "statuslist-left statuslist-right";
        }
    }
</style>
<div>
    <h2><?php echo $L['INDEX.STATUSBLOCKTITLE'] ?>:</h2>

    <div class="wrapper">
        <div class="statuslist-left">
            <h4 id="statusloading"><?php echo $L['INDEX.LOADING'] ?></h4>
            <table id="statusTableLeft" style="width: 100%">

            </table>
        </div>
        <div class="statuslist-right">
            <table id="statusTableRight" style="width: 100%">

            </table>
        </div>
    </div>
</div>


<div data-role="collapsible" id="coll_ebusd_read" data-content-theme="true" data-collapsed="true"
     data-collapsed-icon="carat-d" data-expanded-icon="carat-u" data-iconpos="right"
     class="ui-collapsible ui-collapsible-inset ui-corner-all ui-collapsible-themed-content">
    <h2 class="ui-bar ui-bar-a ui-corner-all ui-collapsible-heading"
        id="ebusd_read">
        <?php echo $L['INDEX.PROPBLOCKTITLE'] ?>
        <span style="font-size:80%;">
        </span>
        <span class="ui-collapsible-heading-status">
            click to expand contents
        </span>
    </h2>

    <a data-role="button"
       href="#"
       id="refreshReadVars"
       target="_blank" data-inline="true" data-mini="true" data-icon="action"
       class="ui-link ui-btn ui-icon-action ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini"
       role="button"><?php echo $L['INDEX.RELOAD'] ?></a>

    <a data-role="button"
       href="#" style="float: right"
       id="refreshReadVars"
       target="_blank" data-inline="true" data-mini="true" data-icon="action"
       class="ui-link ui-btn ui-icon-action ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini"
       role="button" onclick="readAllParams()"><?php echo $L['INDEX.READALL'] ?></a>

    <div id="readBlockBody">

    </div>
</div>

<!-- TEMPLATES -->

<template id="readContainerTemplate">
    <h4 class="read-c-name">unknown</h4>
    <table style="width:100%; padding:8px; border:1px solid #ddd; border-collapse: collapse;">
        <tbody class="read-c-table">
        </tbody>
    </table>
</template>

<template id="readParamTemplate">
    <td class="read-p-state"
        style="text-align:center; background-color:#6DAC20; width:80px; color:white; text-shadow: none;">
        <p class="read-p-state-unknown"><?php echo $L['INDEX.UNKNOWN'] ?></p>
        <p class="read-p-state-ok"><?php echo $L['INDEX.OK'] ?></p>
        <p class="read-p-state-error"><?php echo $L['INDEX.ERROR'] ?></p>
    </td>
    <td class="read-p-name" style="padding-left: 10px;">name</td>
    <td class="read-p-value">value</td>
    <td class="read-p-buttons" style="white-space: nowrap"><a data-role="button"
                                                              href="#"
                                                              target="_blank" data-inline="true" data-mini="true"
                                                              data-icon="action"
                                                              class="ui-link ui-btn ui-icon-action ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini"
                                                              role="button"><?php echo $L['INDEX.READ'] ?></a></td>
</template>

<template id="writeParamTemplate">
    <td class="write-p-state"
        style="text-align:center; background-color:#6DAC20; width:80px; color:white; text-shadow: none;">
        <p class="write-p-state-unknown"><?php echo $L['INDEX.UNKNOWN'] ?></p>
        <p class="write-p-state-ok"><?php echo $L['INDEX.OK'] ?></p>
        <p class="write-p-state-error"><?php echo $L['INDEX.ERROR'] ?></p>
    </td>
    <td class="write-p-name" style="padding-left: 10px;">name</td>
    <td class="write-p-value">
        <input class="write-p-value-input" type="text">
    </td>
    <td><a data-role="button"
           href="#"
           target="_blank" data-inline="true" data-mini="true" data-icon="action"
           class="ui-link ui-btn ui-icon-action ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini"
           role="button"><?php echo $L['INDEX.WRITE'] ?></a></td>
</template>

<template id="writeButton">
    <a data-role="button"
       href="#"
       target="_blank" data-inline="true" data-mini="true" data-icon="action"
       class="ui-link ui-btn ui-icon-action ui-btn-icon-left ui-btn-inline ui-shadow ui-corner-all ui-mini"
       role="button"><?php echo $L['INDEX.WRITE'] ?></a>
</template>

<template id="paramState">
    <p class="p-state-unknown"><?php echo $L['INDEX.UNKNOWN'] ?></p>
    <p class="p-state-ok"><?php echo $L['INDEX.OK'] ?></p>
    <p class="p-state-error"><?php echo $L['INDEX.ERROR'] ?></p>
</template>

<template id="statusItem">
    <tr class="info-item" style="display: block; border:1px solid #ddd; border-collapse: collapse; width: 100%;">
        <td class="info-name" style="width: 100%; white-space: nowrap; padding-right: 20px">name</td>
        <td class="info-value"
            style="white-space: normal; min-width: 110px; padding-right: 10px; border-left:1px solid #ddd; text-align: right;"></td>
        <td class="info-state"
            style="text-align:center; background-color:#6DAC20; min-width:120px; color:white; text-shadow: none;">
        </td>
    </tr>
</template>

<!-- SCRIPTS -->
<script>
    let loadingText = '<?php echo $L['INDEX.LOADING'] ?>';

    let readCircuits;
</script>
<script src="js/index-read.js"></script>
<script src="js/index-write.js"></script>
<script src="js/index-status.js"></script>
<script src="js/index.js"></script>

<?php
// Finally print the footer
LBWeb::lbfooter();
?>
