<?php

namespace ebusd\Api;

require_once './../apiEndpointBase.php';

class EbusdHex extends ApiEndpointBase
{
    public function post(){
        $this->requireRequestParameters(array('hex'));

        $cmd = "ebusctl hex";

        if (isset($this->request->source))
            $cmd = $cmd . ' -s ' . $this->request->source;

        $cmd = $cmd . ' ' . $this->request->hex;
        $result = trim(shell_exec($cmd));

        $this->reply($result);
    }
}

new EbusdHex();